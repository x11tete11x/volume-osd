#!/usr/bin/env python3
import argparse
import confight
import re
import subprocess

def percent_type(arg_value, pat=re.compile('^[1-9][0-9]?|100')):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value

parser = argparse.ArgumentParser()
parser.add_argument('--pamixer-path', help="Set pamixer path")
parser.add_argument('--sudo',help="Use sudo to execute pamixer", action='store_true')
group = parser.add_mutually_exclusive_group()
group.add_argument('-i',type=percent_type, help="Increase volume")
group.add_argument('-d',type=percent_type, help="Increase volume")
group.add_argument('-m',help="Toggle Mute state", action='store_true')
args = parser.parse_args()

pamixer_array=[]
max_volume=100

def setup_args():
    global args
    conf=confight.load_user_app('volume-osd')
    if args.pamixer_path == None:
        args.pamixer_path=conf.get('pamixer_path')
        if args.pamixer_path == None:
            args.pamixer_path=subprocess.check_output(['which','pamixer']).decode(encoding='utf-8').rstrip()
    if not args.sudo:
        args.sudo=conf.get('sudo')

def setup_pamixer_invocation():
    global pamixer_array
    global args
    if args.sudo:
        pamixer_array=['sudo']
    pamixer_array.append(args.pamixer_path)

def initial_setup():
    setup_args()
    setup_pamixer_invocation()

def get_volume():
    pamixer_arg=pamixer_array.copy()
    pamixer_arg.append('--get-volume')
    return(int(subprocess.check_output(pamixer_arg).decode(encoding='utf-8')))

def is_muted():
    pamixer_arg=pamixer_array.copy()
    pamixer_arg.append('--get-mute')
    proc = subprocess.Popen(pamixer_arg,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            )
    std_out, std_err = proc.communicate()
    return (int(proc.returncode) == 0)

def get_percent(arg):
    return(int(float(arg)*100/float(max_volume)))

def notify_send():
    volume_icon="audio-volume-muted"
    if is_muted():
        subprocess.run(['notify-send','-i',volume_icon,'Muted','-h','string:synchronous:volume'])
    else:
        actual_volume=get_volume()
        if 0 < actual_volume < 20:
            volume_icon="audio-volume-low"
        elif 20 <= actual_volume < 60:
            volume_icon="audio-volume-medium"
        elif 60 <= actual_volume:
            volume_icon="audio-volume-high"
        subprocess.run(['notify-send','-i',volume_icon,'"Volume"','-h','int:value:'+str(actual_volume),'-h','string:synchronous:volume'])

def inc_volume(arg):
    pamixer_arg=pamixer_array.copy()
    pamixer_arg.append('-i')
    pamixer_arg.append(arg)
    subprocess.run(pamixer_arg)
    notify_send()

def dec_volume(arg):
    pamixer_arg=pamixer_array.copy()
    pamixer_arg.append('-d')
    pamixer_arg.append(arg)
    subprocess.run(pamixer_arg)
    notify_send()

def toggle_mute():
    pamixer_arg=pamixer_array.copy()
    pamixer_arg.append('-t')
    subprocess.run(pamixer_arg)
    notify_send()


if __name__ == '__main__':
    initial_setup()
    if args.m:
        toggle_mute()
    elif not is_muted():
        if args.i != None:
            inc_volume(args.i)
        else:
            dec_volume(args.d)
    else:
        notify_send()
