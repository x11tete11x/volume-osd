# volume-osd

Tiny script to manipulate volume. Useful for WM like i3

# Dependencies

```
https://github.com/cdemoulins/pamixer
sudo apt install python3-pip libnotify-bin
sudo pip3 install confight
```

# Usage
```
usage: volume-osd.py [-h] [--pamixer-path PAMIXER_PATH] [--sudo] [-v V | -m]

optional arguments:
  -h, --help            show this help message and exit
  --pamixer-path PAMIXER_PATH
                        Set pamixer path
  --sudo                Use sudo to execute pamixer
  -v V                  Increase/Decrease volume by the specified percentage
  -m                    Toggle Mute state

```

# Example i3 config
```
x11tete11x@jarvis:~$ cat .config/i3/config | grep volume
bindsym XF86AudioRaiseVolume exec --no-startup-id /home/x11tete11x/GIT/volume-osd/volume-osd.py -i 5 #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id /home/x11tete11x/GIT/volume-osd/volume-osd.py -d 5 #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id /home/x11tete11x/GIT/volume-osd/volume-osd.py -m # mute sound
```

![alt text](example.png "Running")
